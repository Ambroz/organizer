<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller {

	public function index()
	{
		if (isset($this->loginData->user_group))
		{
			redirect('main');
		}
		else
		{
			$this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
			$this->_viewData['menu'] = $this->load->view('main_menu', $this->_viewData, true);
			$this->_viewData['content'] = $this->load->view('login', $this->_viewData, true);
			$this->load->view('parts/template', $this->_viewData);
		}
	}

	public function login(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		echo $password;die;
		$log = $this->user_model->login($email, $password);

		if (!is_object($log))
		{
			if ($log == 'inactive')
			$this->session->set_flashdata('message', "L'account non è attivo. Visita la tua email per attivare l'account.");
			else
			$this->session->set_flashdata('message', "Impossibile accedere. Controllare la posta elettronica e password immessi.");

			redirect('user');
		}
		else
		{
			$this->session->set_userdata('user', $log);
			redirect('main');
		}
	}

	public function register(){
		$this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
		// $this->_viewData['menu'] = $this->load->view('main_menu', $this->_viewData, true);
		$this->_viewData['content'] = $this->load->view('register', $this->_viewData, true);
		$this->load->view('parts/template', $this->_viewData);
	}
}
