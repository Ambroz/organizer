	<div class="uk-container uk-container-center footer-center">
	<footer class="footer">
		<?php
		if (isset($loginData))
		{
		if($loginData->user_group == 1){//administrator
			?>
			<div class="uk-grid">

			<div class="uk-width-3-5">
				<div class="uk-panel am-visible@666">
				<ul class="uk-navbar uk-navbar-nav left uk-flex uk-flex-middle">
					<li><a href="<?=base_url()?>administrator/main/users/">Mostra utenti</a></li>
					<li><a href="<?=base_url()?>administrator/form">Forme</a></li>
					<li><a href="<?=base_url()?>main/documents/">Genera documento</a></li>
					<li><a href="<?=base_url()?>administrator/main/documents">Documenti generati</a></li>
					<li><a href="<?=base_url()?>account/logout">Logout</a></li>
				</ul>
			</div>


			<div class="uk-panel bot_menu am-hidden@666">
				<a class="tile" href="<?=base_url()?>administrator/main/users/">Mostra utenti</a><br/>
				<a class="tile" href="<?=base_url()?>administrator/form">Forme</a><br/>
				<a class="tile" href="<?=base_url()?>administrator/main/documents">Genera documento</a><br/>
				<a class="tile" href="<?=base_url()?>administrator/main/documents">Documenti generati</a><br/>
				<a class="tile" href="<?=base_url()?>account/logout">Logout</a>
			</div>


		</div>
				<div class="uk-width-2-5 uk-flex uk-flex-middle">
					<div class="uk-panel">
						<ul class="uk-navbar uk-navbar-nav copy">
							<li>Copyright <?=date("Y")?> by Eternal Guardians, Made by <span>Eternal Guardians</span></li>
						</ul>
					</div>
				</div>

			<?php
		}elseif($loginData->user_group == 2){
			?>
			<nav class="uk-navbar">
				<ul class="uk-navbar-nav">
					<li><a href="<?=base_url()?>"><img src='<?=base_url();?>/assets/img/logo.png'></a></li>
					<li><a href="<?=base_url()?>users/projects/">Documents</a></li>
					<li><a href="<?=base_url()?>users/account/">Pay for Document</a></li>
					<li><a href="<?=base_url()?>users/shouts/">Download Document</a></li>
					<li><a href="<?=base_url()?>users/shouts/">Show Generated Document</a></li>
					<li><a href="<?=base_url()?>user/logout">Log Out</a></li>
				</ul>
			</nav>
			<?php
		}elseif($loginData->user_group == 3){//registred user
			?>
			<div class="uk-grid">

			<div class="uk-width-3-5">
				<div class="uk-panel am-visible@666">
				<ul class="uk-navbar uk-navbar-nav left uk-flex uk-flex-middle">
					<li><a href="<?=base_url()?>main/documents/">genera documento</a></li>
					<li><a href="<?=base_url()?>main/my_documents/">I miei documenti</a></li>
					<li><a href="<?=base_url()?>account/change_password">Cambia la password</a></li>
					<li><a href="<?=base_url()?>account/logout">Logout</a></li>
				</ul>
			</div>

			<div class="uk-panel bot_menu am-hidden@666">
				<a class="tile" href="<?=base_url()?>main/documents/">genera documento</a></br>
				<a class="tile" href="<?=base_url()?>main/my_documents/">I miei documenti</a></br>
				<a class="tile" href="<?=base_url()?>account/change_password">Cambia la password</a></br>
				<a class="tile" href="<?=base_url()?>account/logout">Logout</a>
			</div>
		</div>
				<div class="uk-width-2-5 uk-flex uk-flex-middle">
					<div class="uk-panel">
						<ul class="uk-navbar uk-navbar-nav copy">
							<li>Copyright <?=date("Y")?> by Marco, Made by <span>Webtoucan</span></li>
						</ul>
					</div>
				</div>

			<?php
		}
	}else{//unregistred user
			?>
			<!-- <div class="uk-grid">

			<div class="uk-width-3-5">
				<div class="uk-panel not_reg am-visible@666">
				<ul class="uk-navbar uk-navbar-nav left uk-flex uk-flex-middle">
					<li><a href="<?=base_url()?>main/documents/">contrati</a></li>
          <li><a href="<?=base_url()?>main/about/">come funziona</a></li>
          <li><a href="<?=base_url()?>main/contact_us/">Contattaci</a></li>
          <li><a href="<?=base_url()?>account">Login</a></li>
          <li><a class="tile" href="<?=base_url()?>account/signup" style="color: #ff7900;">Registrati</a></li>
				</ul>
			</div>
		</div>
	</div> -->

							<!-- <a class="tm-totop-scroller" data-uk-smooth-scroll="" href="#"></a> -->

				<div class="uk-panel footer-left">
					Footer Lewy
		<img src="" alt=""></div>
	<div class="uk-panel footer-right">
		Footer Prawy
	</div>
			</footer>

	</div>


	<?php
	}
	?>
	</div>
	</div>
	</div>
</footer>
</div>
<script type="text/javascript">
		base_url = '<?=base_url()?>';
</script>
</body>
</html>
