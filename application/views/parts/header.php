<!DOCTYPE html>
<html lang="pl">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		<?php echo (isset($pageTitle)) ? $pageTitle : "EGO"; ?>
	</title>

	<link rel="stylesheet" href="<?=base_url()?>assets/uikit/css/uikit.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/formbuilder/form-builder.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/formbuilder/form-render.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/custom.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/rize-builder.css">
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css">

	<link rel="icon" href="<?=base_url()?>/assets/img/favicon.ico" type="image/ico">

	<script
		src="https://code.jquery.com/jquery-3.2.1.min.js"
		integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
		crossorigin="anonymous"></script>

	<script
		src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
		integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
		crossorigin="anonymous"></script>

	<script src="//cdn.jsdelivr.net/webshim/1.16/polyfiller.js"></script>
	<script>
	    webshims.setOptions('forms-ext', {types: 'date'});
		webshims.polyfill('forms forms-ext');
	</script>

	<!-- <script
		  src="https://code.jquery.com/jquery-2.2.4.min.js"
		  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
		  crossorigin="anonymous"></script> -->

	<script src="<?=base_url()?>assets/uikit/js/uikit.min.js"></script>
	<script src="<?=base_url()?>assets/formbuilder/form-builder.min.js"></script>
	<script src="<?=base_url()?>assets/formbuilder/form-render.min.js"></script>
	<script src="<?=base_url()?>assets/js/custom.js"></script>
	<script src="<?=base_url()?>assets/js/rize-builder.js"></script>
	<script src="<?=base_url()?>assets/js/paypal-button.min.js"></script>

	<!-- tiny MCE -->
	<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
		tinymce.init({
			selector: "#mytextarea",
			plugins: "code paste",
			verify_html: false,
		  	convert_urls: false,
  			paste_as_text: true
		});
	</script>

	<!-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script> -->
	<!-- <script>tinymce.init({ selector:'textarea' });</script> -->

</head>
<body class="home">

<!-- <div class="uk-container uk-container-center"> -->
