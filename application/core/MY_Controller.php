<?php
class GE_Controller extends CI_Controller
{
	protected $_viewData = [];
	protected $loginData = '';

	/**
	* ste footer and header
	* @param none
	* @return constructor
	**/
	function __construct()
	{
		parent::__construct();

		//load header
		$this->_viewData['message'] = $this->session->flashdata('message');
		$this->_viewData['header'] = $this->load->view('parts/header', $this->_viewData, true);
	}
}

class MY_Controller extends GE_Controller
{
	/**
	* ste footer and header
	* @param none
	* @return constructor
	**/
	function __construct()
	{
		parent::__construct();

		$uri = $_SERVER['REQUEST_URI'];
		$s = $this->session->get_userdata();

		$ok = isset($s['user']);

		if($ok)
		{
			$this->loginData = $s['user'];
			$this->_viewData['loginData'] = $this->loginData;
		}

		// if (stripos($uri, 'administrator') !== false && (!isset($this->loginData->user_group) || $this->loginData->user_group != 1))
		// {
		// 	redirect('main');
		// }

		// $this->_viewData['menu'] = $this->load->view('main_menu', $this->_viewData, true);
		$this->_viewData['footer'] = $this->load->view('parts/footer', $this->_viewData, true);

		// getting db enums
		// $groups = $this->db->get('user_group')->result();
		// foreach($groups as $group)
		// 	$group_enum[$group->group_id] = $group->group_name;
		//
		// $this->_viewData['group_enum'] = $group_enum;
		//
		// $this->_viewData['active_enum'] = [
		// 	0 => 'No',
		// 	1 => 'Yes'
		// ];
	}
}
